import React, { Component } from "react";
import FillLevelList from "./FillLevelList";

class VehicleCard extends Component {
  state = {};

  render() {
    let engineSymbol = "/gfx/engine_green.svg";
    let activeSymbol = "/gfx/active_green.svg";
    let cruiseControlSymbol = "/gfx/speed_green.svg";
    let blockedSymbol = "/gfx/stop_red.svg";

    if (!this.props.data.MotorStarted) engineSymbol = "/gfx/engine_black.svg";
    if (!this.props.data.IsActive) activeSymbol = "/gfx/active_red.svg";
    if (!this.props.data.Blocked) blockedSymbol = "/gfx/stop_black.svg";
    if (!this.props.data.CruiseControl.Status == 1)
      cruiseControlSymbol = "/gfx/speed_black.svg";

    let helperIndicator = "";
    if (this.props.data.HelperActive) helperIndicator = "*";

    return (
      <div className="col s12">
        <div className="card  vehicleCard  blue-grey">
          <div className="row iconRow">
            <div className="col s2">
              <span className="title">
                {helperIndicator + this.props.data.Name}
              </span>
            </div>
            <div className="col s3">
              <img src={engineSymbol} />
              &nbsp;
              <img src={activeSymbol} />
              &nbsp;
              <img src={blockedSymbol} />
              &nbsp;
              <img src={cruiseControlSymbol} /> {this.props.data.Speed} km/h
            </div>

            <div className="col s6">
              <FillLevelList data={this.props.data.Fill} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default VehicleCard;
