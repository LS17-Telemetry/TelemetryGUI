import React, { Component } from "react";

class FillTypeCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      alarmValueLower: 0,
      alarmValueUpper: 0
    };

    this.handleLowerChange = this.handleLowerChange.bind(this);
    this.handleUpperChange = this.handleUpperChange.bind(this);
  }

  state = {};

  GetBestPrice(priceList) {
    let maxPrice = 0;
    let bestStation = {};

    for (const price in priceList) {
      if (priceList.hasOwnProperty(price)) {
        const element = priceList[price];

        if (element.Price > maxPrice) {
          maxPrice = element.Price;
          bestStation = element;
        }
      }
    }

    return bestStation;
  }

  handleLowerChange(event) {
    this.setState({ alarmValueLower: event.target.value });
  }

  handleUpperChange(event) {
    this.setState({ alarmValueUpper: event.target.value });
  }

  render() {
    let bestStationText;
    let cardClass = "card  vehicleCard  blue-grey ";

    if (this.props.data.Prices) {
      let bestStation = this.GetBestPrice(this.props.data.Prices);
      bestStationText = bestStation.PriceF + " (" + bestStation.Title + ")";

      if (
        this.state.alarmValueLower > 0 &&
        this.state.alarmValueLower > bestStation.Price
      ) {
        cardClass += " fillCardAlertLower";
      }

      if (
        this.state.alarmValueUpper > 0 &&
        this.state.alarmValueUpper < bestStation.Price
      ) {
        cardClass += " fillCardAlertUpper";
      }
    }

    if (!bestStationText) {
      bestStationText = "";
    }

    return (
      <div className="col s6 m4 ">
        <div className={cardClass}>
          <span className="title">{this.props.data.Title}</span>
          <div className="row">
            <div className="col s3">{"Best Price:"}</div>
            <div className="col s9">
              <span className="price">{bestStationText}</span>
            </div>
          </div>
          <div className="row">
            <div className="col s2">{"Alarm:"}</div>
            <div className="col s5 input-field inline">
              <input
                placeholder="Lower"
                type="number"
                onChange={this.handleLowerChange}
              />
            </div>
            <div className="col s5 input-field inline">
              <input
                placeholder="Upper"
                type="number"
                onChange={this.handleUpperChange}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
//      <p>{JSON.stringify(this.props.data)}</p>

export default FillTypeCard;
