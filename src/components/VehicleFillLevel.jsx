import React, { Component } from "react";

class VehicleFillLevel extends Component {
  state = {};
  render() {
    let levelClass = "lc_Green";

    if (this.props.data.Percent > 85) {
      levelClass = "lc_Red";
    } else if (this.props.data.Percent > 70) {
      levelClass = "lc_Yellow";
    }

    return (
      <span className={levelClass}>
        {this.props.data.Title}: {this.props.data.Percent}% (
        {Math.round(this.props.data.Level)}/
        {Math.round(this.props.data.Capacity)}){", "}
      </span>
    );
  }
}

export default VehicleFillLevel;
