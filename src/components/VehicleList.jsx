import React, { Component } from "react";

import VehicleCard from "./VehicleCard";
import FillTypeCard from "./FillTypeCard";
const axios = require("axios");
//const testData = require("../test/data");
var testData = require("../test/data");
const updateIntervall = 5;
class VehicleList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      currentCount: updateIntervall
    };
    //  this.loadData();
  }

  timer() {
    this.setState({
      currentCount: this.state.currentCount - 1
    });

    if (this.state.currentCount <= 0) {
      this.loadData();
      this.setState({ currentCount: updateIntervall });
    }
  }

  componentDidMount() {
    this.intervalId = setInterval(this.timer.bind(this), 100);
  }

  componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  async getServerData() {
    let r = await axios.get("http://localhost:63700/json");
    return r.data;
  }

  async loadData() {
    let serverRequest = await this.getServerData();
    this.setState({
      data: serverRequest
    });

    /*axios
      .get("http://localhost:63700/json")
      .then(function(result) {
        let s = result.data;
        _this.setState({
          data: result.data
        });
        console.log(result.data);
      });*/
    //  await setTimeout(this.loadData(), 5000);
  }

  render() {
    if (this.state.data) {
      return (
        <div>
          <div className="row">
            {this.state.data.Vehicles.Vehicle.map((item, index) => (
              <VehicleCard key={index} data={item} />
            ))}
          </div>
          <div className="row">
            {this.state.data.FillType.filter(item => item.Prices).map(
              (item, index) => (
                <FillTypeCard key={index} data={item} />
              )
            )}
          </div>
        </div>
      );
    } else {
      console.log("Not yet loaded ");
      return <h1> Not yet loaded {this.state.currentCount}</h1>;
    }
  }
}

export default VehicleList;
